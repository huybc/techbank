const mongoose = require('mongoose');
const Schema = mongoose.Schema;

//define collection
let account = new Schema({
    user_name : {
        type: String
    },
    password : {
        type: String
    }
},{
    collection: 'accounts'
})

module.exports = mongoose.model('account', account)

