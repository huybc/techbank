import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UserandpassLoginComponent } from './userandpass-login.component';

describe('UserandpassLoginComponent', () => {
  let component: UserandpassLoginComponent;
  let fixture: ComponentFixture<UserandpassLoginComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UserandpassLoginComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UserandpassLoginComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
