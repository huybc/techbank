import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FingerprintLoginComponent } from './fingerprint-login.component';

describe('FingerprintLoginComponent', () => {
  let component: FingerprintLoginComponent;
  let fixture: ComponentFixture<FingerprintLoginComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FingerprintLoginComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FingerprintLoginComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
