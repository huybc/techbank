import { BrowserModule } from '@angular/platform-browser';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ChooseLoginComponent } from './components/choose-login/choose-login.component';
import { UserandpassLoginComponent } from './components/userandpass-login/userandpass-login.component';
import { FingerprintLoginComponent } from './components/fingerprint-login/fingerprint-login.component';
import { FaceLoginComponent } from './components/face-login/face-login.component';
import { Route } from '@angular/compiler/src/core';
import { RouterModule, Routes } from '@angular/router';

/* angular material */
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AngularMaterialModule } from './material.module';

//Http service
import { HttpClientModule } from '@angular/common/http'

const routes: Routes = [
  {path: '', pathMatch: 'full', redirectTo: 'choose-login'},
  {path: 'choose-login', component: ChooseLoginComponent},
  {path: 'userandpass-login', component: UserandpassLoginComponent},
  {path: 'fingerprint-login', component: FingerprintLoginComponent},
  {path: 'face-login', component: FaceLoginComponent},

]

@NgModule({
  declarations: [
    AppComponent,
    ChooseLoginComponent,
    UserandpassLoginComponent,
    FingerprintLoginComponent,
    FaceLoginComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    RouterModule.forRoot(routes),
    BrowserAnimationsModule,
    AngularMaterialModule,
    HttpClientModule
  ],
  exports: [RouterModule],
  providers: [],
  bootstrap: [AppComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AppModule { }
