import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MatTableModule } from '@angular/material/table';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatButtonModule } from '@angular/material/button';
import { MatDatepickerModule } from '@angular/material/datepicker';


@NgModule({
   imports: [
      CommonModule,
      MatButtonModule,
    //   MatToolbarModule,
    //   MatIconModule,
    //   MatSidenavModule,
    //   MatBadgeModule,
    //   MatListModule,
    //   MatGridListModule,
      MatFormFieldModule,
      MatInputModule,
    //   MatSelectModule,
    //   MatRadioModule,
      MatDatepickerModule,
    //   MatNativeDateModule,
    //   MatChipsModule,
    //   MatTooltipModule,
      MatTableModule,
    //   MatPaginatorModule
   ],
   exports: [
      MatButtonModule,
    //   MatToolbarModule,
    //   MatIconModule,
    //   MatSidenavModule,
    //   MatBadgeModule,
    //   MatListModule,
    //   MatGridListModule,
      MatInputModule,
      MatFormFieldModule,
    //   MatSelectModule,
    //   MatRadioModule,
      MatDatepickerModule,
    //   MatChipsModule,
    //   MatTooltipModule,
      MatTableModule,
    //   MatPaginatorModule
   ],
   providers: [
      MatDatepickerModule,
   ]
})

export class AngularMaterialModule { }